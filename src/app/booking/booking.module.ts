import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BookingComponent } from './booking.component';
import { ChairComponent } from './chair.component';
import { ListBookingComponent } from './listbooking.component';
import { ListChairComponent } from './listchair.component';



@NgModule({
  imports: [BrowserModule,FormsModule],
  exports: [BookingComponent],
  declarations: [BookingComponent,ListBookingComponent,ListChairComponent,ChairComponent],
  providers: [BookingComponent],
})
export class BookingModule { }
