import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-chair',
  template: `
    <button (click)="addNewBooking(chairItem);setColorBooking()" class="btn btn-danger btn-secondary my-2 w-100" [disabled]="chairItem.trangThai"  [class]='{"btn-danger":chairItem.trangThai,"btn-success":isBooking}'>{{chairItem.tenGhe}}</button>
  `
})

export class ChairComponent implements OnInit {
  @Input() chairItem!:chair;
  @Input() listBooking!:chair[];
  @Output() handleBooking = new EventEmitter<chair>();
  constructor() { }
  isBooking:boolean = false;
  ngOnInit() { }
  addNewBooking(item:chair){
      this.handleBooking.emit(item);
  }
  ngDoCheck(){
    this.setColorBooking();
  }
  setColorBooking(){
     const cloneBooking = [...this.listBooking];
     const index = cloneBooking.findIndex((item)=>{
          return item.soGhe === this.chairItem.soGhe;
     })
     if(index === -1){
        this.isBooking = false;
     }else{
        this.isBooking = true;
     }
  }

}

interface chair {
  soGhe:number,
  tenGhe:string,
  gia:number,
  trangThai:boolean,
}
